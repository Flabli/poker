import java.util.ArrayList;
import java.util.*;

public class Spielfeld {

	private int x;
	private int runde;
	private ArrayList<Karte> flop;
	private ArrayList<Integer> bet;
	private int chipsRunde;
	private int chipsSpiel;
	private Spieler zug;
	private Karte Flop;
	Deck flopdck = new Deck();

	public Spielfeld() {
		x = 0;
		bet = new ArrayList();
		for (int i = 0; i < 15; i++) {
			bet.add(0);
		}
	}

	public void drawFlop(Deck deck1) {
		flop = flopdck.drawMulti(3);
	}

	public void drawTurn() {
		flop.add(flopdck.draw());
	}

	public void drawRiver() {
		flop.add(flopdck.draw());
	}

	public ArrayList<Karte> getFlop() {
		return flop;
	}

	public int getBet(int pos) {
		if (bet.isEmpty() == false) {
			return (bet.get(pos));
		} else {
			return 0;
		}
	}

	public void setBet(int pos, int betrag) {
		bet.set(pos, betrag); // TODO muss die liste noch nach Maxima
								// durchsuchen
	}

	public int getChipsRunde() {
		return chipsRunde;
	}

	public void addChipsRunde(int cr) {
		if(cr>chipsRunde){
		chipsRunde = cr;
		}
		
	}

	public int getChipsSpiel() {
		return chipsSpiel;
	}

	public void addChipsSpiel(int cs) {
		
		chipsSpiel = chipsSpiel + cs;
	}

	public void resetFlop() {
		Deck deck1 = new Deck();
	}

}
