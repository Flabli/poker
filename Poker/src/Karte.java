public class Karte
{
    private int x;
    private char farbe;
    private int wert;

    public Karte(char farbe,int wert){
    this.farbe= farbe;
    this.wert=wert;
    }
    public char getFarbe(){
        return farbe;
    }

    public int getWert(){
        return wert;
    }

}