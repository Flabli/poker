import java.util.ArrayList;
import java.util.*;

public class Deck
{

    private int x;
    public int zufallszahl;
    public ArrayList<String> stapel = new ArrayList();
    public ArrayList<Karte> kartenListe= new ArrayList<Karte>();

    public Deck()
    {
        
        x = 0;
        String dck [] = {"D02","D03","D04","D05","D06","D07","D08","D09","D10","D11","D12","D13","D14","H02","H03","H04","H05","H06","H07","H08","H09","H10","H11","H12","H13","H14","S02","S03","S04","S05",
            "S06","S07","S08","S09","S10","S11","S12","S13","S14","C02","C03","C04","C05","C06","C07","C08","C09","C10","C11","C12","C13","C14"};

        stapel=new ArrayList<>(Arrays.asList(dck));
    }


    public Karte draw()
    {
       zufallszahl=(int)(Math.random()*(stapel.size()));
    
        stapel.get(zufallszahl).charAt(0);
        int a1 = Integer.parseInt(Character.toString((stapel.get(zufallszahl)).charAt(1)));
        int a2 = Integer.parseInt(Character.toString((stapel.get(zufallszahl)).charAt(2)));
        int wert= (a1*10)+a2;

        Karte karteObjekt = new Karte((stapel.get(zufallszahl)).charAt(0),wert);
        return karteObjekt;
    }

    public ArrayList<Karte> drawMulti(int y)
    {
        Karte[] z=new Karte[y+1];
        for(int i=0;i<y;i++){
        z[i]=this.draw();
        kartenListe.add(z[i]);
        stapel.remove(z[i]); //ohne zurücklegen
    }
    
   
    return kartenListe;
    }

    public void reset(){
        String dck [] = {"D02","D03","D04","D05","D06","D07","D08","D09","D10","D11","D12","D13","D14","H02","H03","H04","H05","H06","H07","H08","H09","H10","H11","H12","H13","H14","S02","S03","S04","S05",
            "S06","S07","S08","S09","S10","S11","S12","S13","S14","C02","C03","C04","C05","C06","C07","C08","C09","C10","C11","C12","C13","C14"};
    stapel=new ArrayList<>(Arrays.asList(dck));   
    }
}


